import time
import schedule
from pytz import timezone
from datetime import datetime

from db import Log
from main import SearchEngine


CURRENT_TZ = timezone('Europe/Minsk')
SEARCH_JOB = 'Search job'

def time_control(func):
    def wrapped():
        now = datetime.now().astimezone(CURRENT_TZ)
        print(f"Start job at - {now}")
        func()
        now = datetime.now().astimezone(CURRENT_TZ)
        Log.update(last_job_at=now).where(Log.message == SEARCH_JOB).execute()
        print(f"End job at - {now}\n")

    return wrapped


@time_control
def job():
    response = SearchEngine.run()

    if not response == SearchEngine.EMPTY:
        Log.create(message=response, last_job_at=datetime.now().astimezone(CURRENT_TZ))


Log.create(message=SEARCH_JOB, last_job_at=datetime.now().astimezone(CURRENT_TZ))
schedule.every(2).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
