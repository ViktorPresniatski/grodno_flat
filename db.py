from peewee import *


db = SqliteDatabase('sqlite.db')


class Announcement(Model):
    date = CharField(null=True)
    body = CharField(null=True)

    class Meta:
        database = db


class Log(Model):
    message = CharField(null=True)
    last_job_at = DateTimeField()

    class Meta:
        database = db


def create_db():
    Announcement.drop_table()
    Announcement.create_table()
    Log.drop_table()
    Log.create_table()


if __name__ == '__main__':
    create_db()
