import requests
from db import Announcement, create_db
from telegram_sender import TelegramSender
from bs4 import BeautifulSoup


class AnnouncementBot:
    SEARCH_URL = 'http://grodno.gov.by/ru/main.aspx?guid=1021'

    def initialize_db(self):
        data = self.search()
        Announcement.insert_many(data).execute()

    def get_new_announcement(self):
        data = self.search()
        latest_an = data[0]

        try:
            an = Announcement.select().order_by(Announcement.id.desc()).get()
        except Exception:
            return latest_an

        if an.date != latest_an.get('date') or an.body != latest_an.get('body'):
            return latest_an

        return None

    def search(self):
        response = requests.get(url=self.SEARCH_URL)

        if not response.ok:
            raise Exception("FAILED RESPONSE")

        return self.parse_response(response)

    def parse_response(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')
        table = soup.find('div', {'class': 'txt'}).table.select('td')
        table = [row for row in table if row.get_text()]
        return [{'date': row.span.get_text(), 'body': row.b.get_text()} for row in table]


class SearchEngine:
    EMPTY = 'EMPTY'

    @classmethod
    def run(cls):
        bot = AnnouncementBot()
        try:
            announcement = bot.get_new_announcement()
        except Exception as e:
            return f'Error: {str(e)}'

        if not announcement:
            return cls.EMPTY

        response = cls.send_notification(announcement)
        if not response:
            return 'Telegram was failed'

        Announcement.create(**announcement)
        return f"Sent {announcement['date']} - {announcement['body']}"

    @classmethod
    def send_notification(cls, announcement):
        return TelegramSender.send_notification(announcement)


if __name__ == '__main__':
    create_db()
    AnnouncementBot().initialize_db()
    SearchEngine.run()
