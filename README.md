# Bot for search flat in Grodno

## Install requirements:
1. virtualenv venv -p python3
2. source venv/bin/activate
3. pip install -r requirements.txt
4. fill `settings.json` file from `settings.json.example`

## Initialize db and run test:
1. python db.py
3. python main.py

## Run scheduler:
1. python db.py
2. python scheduler.py
