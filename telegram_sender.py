import os
import json
import telegram

SETTINGS_FILE_NAME = 'settings.json'
dir_path = os.path.dirname(os.path.realpath(__file__))
ABS_PATH = os.path.join(dir_path, SETTINGS_FILE_NAME)

def read():
    with open(ABS_PATH) as f:
        json_data = json.load(f)

    return json_data


settings = read()

TOKEN = settings.get('token')
RECEIVERS = settings.get('receivers')
bot = telegram.Bot(token=TOKEN)


class TelegramSender:

    @classmethod
    def send_notification(cls, announcement):
        message = cls.make_message(announcement)
        ret = []
        for receiver in RECEIVERS:
            try:
                bot.send_message(chat_id=receiver, text=message)
                ret.append(True)
            except Exception:
                ret.append(False)

        return all(ret)

    @classmethod
    def make_message(cls, an):
        return f'There is new announcement: {an["date"]} - {an["body"]}. ' \
                'Check the desk: http://grodno.gov.by/ru/main.aspx?guid=1021'
